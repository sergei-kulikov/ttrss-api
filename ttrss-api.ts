/**
 * Created by Sergei on 08.05.2015.
 */

const agent = require('superagent');

class TtrssApi {

    private url: string;
    private sid: string;
    private delay: number;
    private thread: Promise<any>;

    constructor(url: string, delay: number = 2 * 1000) {
        this.url = url;
        this.thread = Promise.resolve();
        this.delay = delay;
    }

    private request(op: string, data: any = {}): Promise<any> {
        var url = this.url;
        data.op = op;
        if (this.sid) {
            data.sid = this.sid;
        }
        var action = this.thread
            .then(() => new Promise((resolve, reject) => {
                agent.post(url)
                    .send(data)
                    .end(function(err, res) {
                        err ? reject(err) : resolve(res);
                    });
            }))
            .then((response: {status: number; text: string;}) => {
                if (response.status != 200) {
                    return Promise.reject(response);
                }
                return JSON.parse(response.text);
            })
            .then((body) => {
                if (body.status !== 0) {
                    return Promise.reject(body.content.error);
                }
                return body.content;
            });
        this.thread = action.then(() => this.wait(), () => this.wait());
        return action;
    }

    private wait():Promise<undefined> {
        return new Promise(resolve => {
            setTimeout(resolve, this.delay);
        });
    }

    /**
     * getApiLevel (since 1.5.8, api level 1)
     *
     * Return an abstracted integer API version level, increased with each API functionality change.
     * This is the proper way to detect host API functionality, instead of using getVersion.
     * {"level":1}
     *
     * Whether tt-rss returns error for this method (e.g. 1.5.7 and below) client should assume API level 0.
     */
    getApiLevel(): Promise<dataApiLevel> {
        return this.request('getApiLevel');
    }


    /**
     * getVersion
     *
     * Returns tt-rss version.
     * As of, 1.5.8 it is not recommended to use this to detect API functionality, please use getApiLevel instead.
     * {"version":"1.4.0"}
     */
    getVersion(): Promise<dataVersion> {
       return this.request('getVersion');
    }


    /**
     * login
     *
     * Parameters:
     *
     * user (string)
     * password (string)
     *
     * Returns client session ID.
     * {"session_id":"xxx"}
     *
     * It can also return several error objects:
     *
     * If API is disabled for this user: {"error":"API_DISABLED"}
     * If specified username and password are incorrect: {"error":"LOGIN_ERROR"}
     *
     * In case it isn't immediately obvious,
     * you have to login and get a session ID even if you are using single user mode.
     * You can omit user and password parameters.
     *
     * On 1.6.0 and above login also returns current API level as an api_level integer,
     * you can use that instead of calling getApiLevel after login.
     */
    login(params: paramLogin): Promise<dataSession> {
        return this.request('login', params)
            .then((session:dataSession) => {
                this.sid = session.session_id;
                return session;
            });
    }


    /**
     * logout
     *
     * Closes your login session.
     * Returns either status-message {"status":"OK"} or an error (e.g. {"error":"NOT_LOGGED_IN"})
     */
    logout(): Promise<dataStatus> {
        return this.request('logout');
    }


    /**
     * isLoggedIn
     *
     * Returns a status message with boolean value
     * showing whether your client (e.g. specific session ID) is currently logged in.
     * {"status":false}
     */
    isLoggedIn(): Promise<dataLoggedStatus> {
        return this.request('isLoggedIn');
    }


    /**
     * getUnread
     *
     * Returns an integer value of currently unread articles.
     * {"unread":"992"}
     */
    getUnread(): Promise<dataUnreadCount> {
        return this.request('getUnread');
    }


    /**
     * getCounters
     *
     * Returns JSON-encoded counter information. Requires 1.5.0.
     *
     * output_mode (string, default: flc) - what kind of information to return
     *     f - feeds,
     *     l - labels,
     *     c - categories,
     *     t - tags
     *
     */
    getCounters(params: paramGetCounters): Promise<dataFeedCounter[]> {
        return this.request('getCounters', params);
    }


    /**
     * getFeeds
     *
     * Returns JSON-encoded list of feeds. The list includes category id, title, feed url, etc.
     *
     * Parameters:
     *
     * cat_id (integer) - return feeds under category cat_id
     * unread_only (bool) - only return feeds which have unread articles
     * limit (integer) - limit amount of feeds returned to this value
     * offset (integer) - skip this amount of feeds first
     * include_nested (bool) - include child categories (as Feed objects with is_cat set) requires 1.6.0
     *
     * Pagination:
     *
     * Limit and offset are useful if you need feedlist pagination.
     * If you use them, you shouldn't filter by unread, handle filtering in your app instead.
     *
     * Special category IDs are as follows:
     *
     * 0 - Uncategorized
     * -1 - Special (e.g. Starred, Published, Archived, etc.)
     * -2 - Labels
     *
     * Added in 1.5.0:
     *
     * -3 - All feeds, excluding virtual feeds (e.g. Labels and such)
     * -4 - All feeds, including virtual feeds
     *
     * Known bug: Prior to 1.5.0 passing null or 0 cat_id to this method
     * returns full list of feeds instead of Uncategorized feeds only.
     */
    getFeeds(params: paramGetFeeds): Promise<dataFeed[]> {
        return this.request('getFeeds', params);
    }

    /**
     * getCategories
     *
     * Returns JSON-encoded list of categories with unread counts.
     *
     * unread_only (bool) - only return categories which have unread articles
     * enable_nested (bool) - switch to nested mode, only returns topmost categories requires 1.6.0
     * include_empty (bool) - include empty categories requires 1.7.6
     *
     * Nested mode in this case means that a flat list of only topmost categories is returned
     * and unread counters include counters for child categories.
     *
     * This should be used as a starting point,
     * to display a root list of all (for backwards compatibility)
     * or topmost categories, use getFeeds to traverse deeper.
     */
    getCategories(params: paramGetCategories): Promise<dataCategory[]> {
        return this.request('getCategories', params);
    }


    /**
     * getHeadlines
     *
     * Returns JSON-encoded list of headlines.
     *
     * Parameters:
     *
     * feed_id (integer) - only output articles for this feed
     * limit (integer) - limits the amount of returned articles (see below)
     * skip (integer) - skip this amount of feeds first
     * filter (string) - currently unused (?)
     * is_cat (bool) - requested feed_id is a category
     * show_excerpt (bool) - include article excerpt in the output
     * show_content (bool) - include full article text in the output
     * view_mode (string = all_articles, unread, adaptive, marked, updated)
     * include_attachments (bool) - include article attachments (e.g. enclosures) requires 1.5.3
     * since_id (integer) - only return articles with id greater than since_id requires 1.5.6
     * include_nested (boolean) - include articles from child categories requires 1.6.0
     * order_by (string) - override default sort order requires 1.7.6
     * sanitize (bool) - sanitize content or not requires 1.8 (default: true)
     * force_update (bool) - try to update feed before showing headlines requires 1.14 (api 9) (default: false)
     * has_sandbox (bool) - indicate support for sandboxing of iframe elements (api 10) (default: false)
     *
     * Limit:
     *
     * Before API level 6 maximum amount of returned headlines is capped at 60, API 6 and above sets it to 200.
     *
     * This parameters might change in the future (supported since API level 2):
     *
     * search (string) - search query (e.g. a list of keywords)
     * search_mode (string) - all_feeds, this_feed (default), this_cat (category containing requested feed)
     * match_on (string) - ignored
     *
     * Special feed IDs are as follows:
     *
     * -1 - starred
     * -2 - published
     * -3 - fresh
     * -4 - all articles
     * 0 - archived
     * IDs < -10 - labels
     *
     * Sort order values:
     *
     * date_reverse - oldest first
     * feed_dates - newest first, goes by feed date
     * (nothing) - default
     *
     */
    getHeadlines(params: paramGetHeadlines): Promise<dataHeadline[]> {
        return this.request('getHeadlines', params);
    }


    /**
     * updateArticle
     *
     * Update information on specified articles.

     * Parameters:

     article_ids (comma-separated list of integers) - article IDs to operate on
     mode (integer) - type of operation to perform (0 - set to false, 1 - set to true, 2 - toggle)
     field (integer) - field to operate on (0 - starred, 1 - published, 2 - unread, 3 - article note since api level 1)
     data (string) - optional data parameter when setting note field (since api level 1)

     E.g. to set unread status of articles X and Y to false use the following:

     ?article_ids=X,Y&mode=0&field=2

     Since version:1.5.0 returns a status message:

     {"status":"OK","updated":1}

     “Updated” is number of articles updated by the query.

     */
    updateArticle(params: paramUpdateArticle): Promise<dataUpdateArticleResult> {
        return this.request('updateArticle', params);
    }

}

// TypeDef Params
// =====================================================================================================================

interface paramLogin {
    user: string;
    password: string;
}


interface paramGetCounters {
    output_mode?: string;
}


interface paramGetFeeds {
    cat_id: number; // (integer) - return feeds under category cat_id
    unread_only?: boolean; // - only return feeds which have unread articles
    limit?: number; // (integer) - limit amount of feeds returned to this value
    offset?: number; // (integer) - skip this amount of feeds first
    include_nested?: boolean; // - include child categories (as Feed objects with is_cat set) requires 1.6.0
}


interface paramGetCategories {
    unread_only: boolean;
    enable_nested: boolean;
    include_empty: boolean;
}


interface paramGetHeadlines {
    feed_id: number; // (integer) - only output articles for this feed
    limit: number; // (integer) - limits the amount of returned articles (see below)
    skip: number; // (integer) - skip this amount of feeds first
    filter: string; // - currently unused (?)
    is_cat: boolean; // - requested feed_id is a category
    show_excerpt: boolean; // - include article excerpt in the output
    show_content: boolean; // - include full article text in the output
    view_mode: string; // TODO: enum {all_articles, unread, adaptive, marked, updated}
    include_attachments: boolean; // - include article attachments (e.g. enclosures) requires 1.5.3
    since_id: number; // (integer) - only return articles with id greater than since_id requires 1.5.6
    include_nested: boolean; // - include articles from child categories requires 1.6.0
    order_by: string; // - override default sort order requires 1.7.6
    sanitize: boolean; // - sanitize content or not requires 1.8 (default: true)
    force_update: boolean; // - try to update feed before showing headlines requires 1.14 (api 9) (default: false)
    has_sandbox: boolean; // - indicate support for sandboxing of iframe elements (api 10) (default: false)
}

interface paramUpdateArticle {
    article_ids: string; // (comma-separated list of integers) - article IDs to operate on
    mode: number; // mode (integer) - type of operation to perform (0 - set to false, 1 - set to true, 2 - toggle)
    field: number; // field (integer) - field to operate on (0 - starred, 1 - published, 2 - unread, 3 - article note since api level 1)
    data: string; // data (string) - optional data parameter when setting note field (since api level 1)
}


// TypeDef Data
// =====================================================================================================================

interface dataApiLevel {
    level: number;
}

interface dataVersion {
    version: string;
}

interface dataSession {
    session_id: string;
}

interface dataStatus {
    status: string;
}


interface dataLoggedStatus {
    status: boolean
}


interface dataUnreadCount {
    unread: string;
}


interface dataFeedCounter {
    id: string | number;
    kind: string; // enum {'cat'}
    counter: number;
    updated?: string;
    has_img?: number; // enum {0, 1}
    error?: string;
}


interface dataFeed {
    feed_url: string;
    title: string;
    id: number;
    unread: number;
    has_icon: boolean;
    cat_id: number;
    last_updated: number; // unix_timestamp
    order_id: number;
}


interface dataCategory {
    id: string;
    title: string;
    unread: number;
    order_id?: number;
}


interface dataHeadline {
    id: number;
    unread: boolean;
    marked: boolean;
    published: boolean;
    updated: number; // unix_timestamp
    is_updated: boolean;
    title: string;
    link: string;
    feed_id: string;
    tags: string[];
    labels: string[][]; // first row contains labels
    feed_title: string;
    comments_count: number;
    comments_link: string;
    always_display_attachments: boolean;
    author: string;
    score: number;

    excerpt?: string;
    content?: string; // HTML
    attachments: any[]; //TODO: type
}


interface dataUpdateArticleResult {
    status: string,
    updated: number
}

// Export
// =====================================================================================================================

export = TtrssApi;
